# Wikidata Movie Browser
Wikidata Movie Browser is an application that allows for simple movie searching, filtering and
recommendation receiving based on multiple criteria that wouldn't be parametrized by a regular search
system. It is written in Python and uses Streamlit framework to present interactive interface to the user.
It is built on top of open-source Wikidata knowledge graph and utilizes SPARQL for data querying
through Wikidata SPARQL endpoint.

## Streamlit cloud application
https://wikidata-movie-browser-zth6zcqpqqvfcbwgg8bebx.streamlit.app/

## Run locally
```
git clone git@gitlab.com:mjagos/wikidata-movie-browser.git
cd wikidata-movie-browser
pip install -r requirements.txt
streamlit run .\app\wikidata_movie_browser.py
```

## Implementation
The application sits on top of WikiData SPARQL Endpoint and retrieves RDF triples from it in JSON-LD format. 
The following workflow is executed each time user changes any of the parameters:

![image info](document/diagram.jpg)
